
Feature('Claim test');

Scenario('test desktop click clear field', (I) => {
    let field = '#react-select-2--value > div.Select-input > input';
    I.amOnPage('/');
    I.fillField(field, 'Vilnius');
    I.waitForVisible('#react-select-2--option-0');
    I.click('#react-select-2--option-0');
    I.click('.clear-value');
    I.seeInField(field, '');
    I.saveScreenshot('screen_click.jpg');
});

Scenario('test mobile device touch clear field', (I) => {
    let field = '#react-select-2--value > div.Select-input > input';
    I.amOnPage('/');
    I.fillField(field, 'Vilnius');
    I.waitForVisible('#react-select-2--option-0', 5);
    I.click('#react-select-2--option-0');
    I.touch('.clear-value');
    I.seeTextEquals('', '#react-select-2--value-item');
    I.saveScreenshot('screen_touch.jpg');
});
    