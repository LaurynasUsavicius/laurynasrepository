
'use strict';

class WebDriverIOHelper extends Helper {
  touch(selector) {
    return this.helpers['WebDriverIO'].browser.touch(selector);
  }
}

module.exports = WebDriverIOHelper;
